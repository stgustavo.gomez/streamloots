import React, { useEffect, useState } from "react";
import Skeleton from "./components/Skeleton";
import List from "./components/List";

const ENDPOINT = './fakeapi/cards.json'

function App() {


  function sendEvent(eventName: String, eventProperties: Object){
        console.log('analytics 1', { eventName, eventProperties, });
  }

  
  const [cards, setCards] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      const result = await fetch (ENDPOINT);
        const data = await result.json();

        setCards(data);

        console.log(data);
    };

      fetchData();

  }, []);


  const[inputCardName, setInputCardName] = useState('');

  function handleChange(event){
    setInputCardName(event.target.value);
  }

  const filteredCards = cards.filter(u => u.name.toLowerCase().includes(inputCardName.toLocaleLowerCase()));

  return (
    <Skeleton>
      <div>
            <label htmlFor="filter-username" className="block text-sm font-medium text-gray-700">Search Card:</label>
            <div className="mt-1 relative rounded-md shadow-sm">
                <input onChange={handleChange} type="text" name="filter-username" id="filter-username" className="block w-full pr-10 pl-4 h-12 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm rounded-md" placeholder="Card" value={inputCardName} />
                
            </div>
               {inputCardName != inputCardName.toUpperCase() || inputCardName == '' ? <p id="error-text" className="mt-2 text-sm text-red-600"></p> : <p id="error-text" className="mt-2 text-sm text-red-600">Please, use only lowercase.</p> }
        </div>

      <List sendEvent={sendEvent} cards={filteredCards} />
    </Skeleton>
  );
}

export default App;
