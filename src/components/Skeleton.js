import React from "react";

const Skeleton = ({ children }) => (
  <div className="bg-gray-900 min-h-screen">
    <div className="mx-auto py-12 px-4 max-w-7xl sm:px-6 lg:px-8 lg:py-24">
      <div className="space-y-12">{children}</div>
    </div>
  </div>
);

export default Skeleton;
