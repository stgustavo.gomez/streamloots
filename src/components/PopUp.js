import React, {useState} from "react";
import { ImCross } from 'react-icons/im';


const PopUp:React.FC<{editCardInfo: Function, hidePopUp: Function, sendEvent: Function}> = ({editCardInfo, hidePopUp, sendEvent}) => {

    const [newCardName, addCardName] = useState('');

    const [newImageURL, addImageUrl] = useState('');

    function handleCardName (event: Object) {
        addCardName(event.target.value);
    }

    function handleImageURL (event: Object) {
        addImageUrl(event.target.value);
    }

    function closePopUp (event: Object){
        hidePopUp();
        sendEvent(event.type, event.target);
    }

    function addNewCardInfo (event: Object) {    
        console.log(event);
        editCardInfo(newCardName, newImageURL);
        sendEvent(event.type, event.target);
    }

    return(
        <div className="popUpCont">
            <div className="popUp">
                <p>Introduce un nombre nuevo</p>
                <input onChange={handleCardName} value={newCardName}></input>
                <p className="secondText">Introduce una URL para la imagen nueva</p>
                <input onChange={handleImageURL} value={newImageURL}></input>

                <button className="submitButton" onClick={addNewCardInfo}>Editar</button>

                <button className="closePopUp" onClick={closePopUp}><ImCross/></button>

            </div>
        </div>
    )
}

export default PopUp;