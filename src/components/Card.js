import React, {useState} from "react";
import { ImBin, ImPencil } from 'react-icons/im';


const Card:React.FC<{name: String, imageUrl: String}> = ({name = 'Nombre', imageUrl = 'https://static.streamloots.com/e19c7bf6-ca3e-49a8-807e-b2e9a1a47524/753c3953-309f-4141-9d7e-078214efca62.png', showPopUp, _id, sendEvent}) =>{

    const [elementDeleted, ToggleElement] = useState(false);

    function DeleteElement (event: Object) {
        ToggleElement(true);
        sendEvent(event.type, event.target);
    }

    //console.log(_id);

    function editCard (event: Object){
        showPopUp(_id);
        sendEvent(event.type, event.target);
    }

    const CardComponent = () => {
        return (
            <div className="cardCont">
                <div className="cardName">{name}</div>
                <img className="cardImage" src={imageUrl}></img>
                <div className="cardButtons">
                    <button onClick={DeleteElement}><ImBin/></button>
                    <button onClick={editCard}><ImPencil/></button>
                </div>
                
            </div>
        )
    }

    return(
        <>
           {elementDeleted == false ? <CardComponent/> : null}
        </>
    )
}

export default Card;