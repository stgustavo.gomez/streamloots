import { func } from "prop-types";
import React, {useState} from "react";
import Card from "./Card";
import PopUp from "./PopUp"


const List:React.FC<{cards: Array, sendEvent: Function}> = ({ cards = [], sendEvent}) => {


  const [cardKey, toggleCardKey] = useState('');

  const [popUpShown, togglePopUp] = useState(false);

  
  
  function editCardInfo (cardName: String, imgUrl: String){

    let fileExtension = imgUrl.split('.').pop();

    for(let i = 0; i < cards.length; i++)
        if(cards[i]._id == cardKey){

            if(cardName != '')
              cards[i].name = cardName;
            
            if(imgUrl != '' && (fileExtension == 'jpeg' || fileExtension == 'jpg' || fileExtension == 'png' || fileExtension == 'gif'))
              cards[i].imageUrl = imgUrl;
        }


    hidePopUp();
  }

  //editCardInfo();

  function showPopUp (key: String){
    togglePopUp(true);
    toggleCardKey(key);
  }

  function hidePopUp (){
    togglePopUp(false);
  }

  return (
    <div
      className={`dark sm:grid sm:grid-cols-2 sm:gap-6 sm:space-y-0 md:grid-cols-4 lg:grid-cols-6 lg:gap-8`}
      data-testid="list"
    >
      {cards.map((card) => (<Card sendEvent={sendEvent} showPopUp={showPopUp} {...card} key={card._id}/>))}

     {popUpShown == true ? <PopUp sendEvent={sendEvent} hidePopUp={hidePopUp} editCardInfo={editCardInfo}/> : null}
         
    </div>
  );
};

export default List;
