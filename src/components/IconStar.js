import React from "react";
import Icon from "./Icon";

const classActive = "text-yellow-500";
const classInactive = "text-gray-900 dark:text-white";

export default function IconStar({ isActive }) {
  const currentClass = isActive ?classActive:classInactive;
  return (
    <button>
      <Icon
        className={`${currentClass} flex hover:text-yellow-500 dark:hover:text-yellow-500`}
        name="star"
      />
    </button>
  );
}
