import React, { useEffect, useState } from "react";


const Filter = () => {
    return(
        <div>
            <label htmlFor="filter-username" className="block text-sm font-medium text-gray-700">Search by username:</label>
            <div className="mt-1 relative rounded-md shadow-sm">
                <input onChange="" type="text" name="filter-username" id="filter-username" className="block w-full pr-10 pl-4 h-12 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm rounded-md" placeholder="@username" value="" />
                <div id="error-icon" className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                <svg className="h-5 w-5 text-red-500" xmlns="<http://www.w3.org/2000/svg>" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clipRule="evenodd" />
                </svg>
                </div>
            </div>
                <p id="error-text" className="mt-2 text-sm text-red-600">Please, use only lowercase.</p>
        </div>

    );
    };

export default Filter;